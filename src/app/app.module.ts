import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PantallaComponent } from 'src/app/components/pantalla/pantalla.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';



import {
  ModalAltaMotivoDesaplicacionComponent
} from './components/modal-alta-motivo-desaplicacion/modal-alta-motivo-desaplicacion.component';
import { FormsModule } from '@angular/forms';
import { FrameworkModule} from '@next/nx-core';
import { CommonsModule} from '@next/nx-controls-common';
import { CommonModule } from '@angular/common';
import { HomeComponent } from 'src/app/components/home/home.component';

import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatInputModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule, MatRadioModule,
  MatDatepickerModule, MatNativeDateModule, MatTableModule, MatIconModule, MatButtonToggleModule, MatSlideToggleModule,
  MatTabsModule, MatCheckboxModule, MatMenuModule, MatStepperModule, MatPaginatorModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MotivoDesaplicacionComponent } from 'src/app/components/motivo-desaplicacion/motivo-desaplicacion.component';

import { MatFormFieldModule} from '@angular/material';
import { MatTreeModule } from '@angular/material/tree';

import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { DesaplicacionPagoComponent } from 'src/app/components/desaplicacion-pago/desaplicacion-pago.component';
import {
  ModalComentarioAutorizarRechazarComponent
} from './components/modal-comentario-autorizar-rechazar/modal-comentario-autorizar-rechazar.component';
import { ReporteDesaplicacionPagoComponent } from './components/reporte-desaplicacion-pago/reporte-desaplicacion-pago.component';
import { BitacoraDesaplicacionPagoComponent } from './components/bitacora-desaplicacion-pago/bitacora-desaplicacion-pago.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { JuegoComponent } from './components/juego/juego.component';

import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import { MiLoginComponent } from './components/mi-login/mi-login.component';
import { NoAutorizadoComponent } from './components/no-autorizado/no-autorizado.component';
import {MatCardModule} from '@angular/material/card';




const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '',
  application: 'DESAPLICACIONREVERSA',
  applicationTitle: 'Desaplicacion de Pagos'
};


@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    PantallaComponent,
    HomeComponent,
    MotivoDesaplicacionComponent,
    ModalAltaMotivoDesaplicacionComponent,
    DesaplicacionPagoComponent,
    ModalComentarioAutorizarRechazarComponent,
    ReporteDesaplicacionPagoComponent,
    BitacoraDesaplicacionPagoComponent,
    EmpleadosComponent,
    JuegoComponent,
    MiLoginComponent,
    NoAutorizadoComponent
  ],
  entryComponents: [ModalAltaMotivoDesaplicacionComponent, ModalComentarioAutorizarRechazarComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot(),
    MatTabsModule,
    FormsModule,
    CdkTableModule,
    CdkTreeModule,
    MatInputModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule, MatRadioModule,
    MatDatepickerModule, MatNativeDateModule, MatTableModule, MatIconModule, MatButtonToggleModule, MatSlideToggleModule,
  MatTabsModule, MatCheckboxModule, MatMenuModule, MatStepperModule, MatPaginatorModule,
  MatExpansionModule,
  MatFormFieldModule,
  ReactiveFormsModule,
  MatListModule,
  MatTreeModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatGridListModule,
  MatButtonModule,
  MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
