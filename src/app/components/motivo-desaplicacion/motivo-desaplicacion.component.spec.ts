import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotivoDesaplicacionComponent } from './motivo-desaplicacion.component';

describe('MotivoDesaplicacionComponent', () => {
  let component: MotivoDesaplicacionComponent;
  let fixture: ComponentFixture<MotivoDesaplicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotivoDesaplicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotivoDesaplicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
