import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatCheckboxChange } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListadoReporteReversas } from 'src/app/models/listado-reporte-reversa';
import { ExcelService } from 'src/app/services/excel-service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XLSXStyle from 'xlsx-style';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Component({
  selector: 'app-reporte-desaplicacion-pago',
  templateUrl: './reporte-desaplicacion-pago.component.html',
  providers: [DesaplicacionPagoService, ExcelService]
})
export class ReporteDesaplicacionPagoComponent implements OnInit {
  title: string;
  formulario: FormGroup;
  credito: string;
  fechaInicio: string;
  fechaFin: string;
  spinner: boolean;
  isHiddenBtnGenExcel = true;
  displayedColumns: string[] = ['credito', 'idcliente', 'cliente',
  'tipolinea', 'linea', 'cuenta', 'fechaoriginal', 'cantidadoriginal', 'fechareversa',
   'Cantidadreversado', 'tipomovimiento', 'describemov', 'amortizacion',
   'fechapagoamortizacion', 'fechavencimiento', 'statusamortizacion', 'usuario',
   'numerotransaccion', 'clasificacion' ];
   public dataSourceReporteReversa: MatTableDataSource<ListadoReporteReversas>;

   elementoFiltroReporte  = {
    credito: '',
    fechaInicio: '',
    fechaFin: ''
  };
  public listadoReporteReversa: ListadoReporteReversas;
  public listadolistadoReporteReversa: ListadoReporteReversas[];
  constructor(private fb: FormBuilder, private alertServices: AlertService,
              private desaplicacionPagoService: DesaplicacionPagoService,
              private cdr: ChangeDetectorRef,
              public dialog: MatDialog,
              private excelService: ExcelService) {
      this.formulario = this.fb.group({
      credito: ['', [Validators.minLength(15), Validators.maxLength(16)]],
      fechaInicio: [''],
      fechaFin: ['' ]
  });
}

  ngOnInit() {
    this.title = 'Desaplicacion Pago - Reporte';
  }
  traeReporteReversa() {

    if (this.credito !== '' && this.credito !== undefined) {
    this.elementoFiltroReporte.credito = this.credito;
    } else {
      this.elementoFiltroReporte.credito = null;
    }
    if (this.fechaInicio !== '' && this.fechaInicio !== undefined &&
        this.fechaInicio !== null) {
      this.elementoFiltroReporte.fechaInicio = this.convertirFechas(this.fechaInicio);
    } else {
      this.elementoFiltroReporte.fechaInicio = null;
    }

    if (this.fechaFin !== '' && this.fechaFin !== undefined &&
        this.fechaFin !== null) {
      this.elementoFiltroReporte.fechaFin = this.convertirFechas(this.fechaFin);
    } else {
      this.elementoFiltroReporte.fechaFin = null;
    }
    this.desaplicacionPagoService.ConsultaReporteReversa(this.elementoFiltroReporte).subscribe(
      reponse => {
        console.log(reponse);
        if (reponse.length > 0) {
          // formateamos las fechas
          this.listadolistadoReporteReversa = reponse;
          this.listadolistadoReporteReversa.forEach(elemento => {
            elemento.fechaOriginal = elemento.fechaOriginal.substring(0, 10);
            elemento.fechaPagoAmortizacion = elemento.fechaPagoAmortizacion.substring(0, 10);
            elemento.fechaReversa = elemento.fechaReversa.substring(0, 10);
            elemento.fechaVencimiento = elemento.fechaVencimiento.substring(0, 10);
          });
          this.dataSourceReporteReversa =
          new MatTableDataSource<ListadoReporteReversas>(this.listadolistadoReporteReversa);
          this.isHiddenBtnGenExcel = false;
          this.alertServices.success('Consulta exitosa');
        } else {
          this.alertServices.error('No se encontro informacion con los datos proporcionados');
        }
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          // this.resultsLength = 0;
          this.dataSourceReporteReversa =
          new MatTableDataSource<ListadoReporteReversas>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          this.dataSourceReporteReversa =
          new MatTableDataSource<ListadoReporteReversas>();
          // this.resultsLength = 0;
          this.alertServices.error(error.error.message);
        }
      }
    );
  }
  convertirFechas(parametro) {
    console.log(parametro);
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-') + 'T00:00:00.000Z';
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: {}, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private wrapAndCenterCell(cell: XLSX.CellObject) {
    const wrapAndCenterCellStyle = { alignment: { wrapText: true, vertical: 'center', horizontal: 'center' } };
    this.setCellStyle(cell, wrapAndCenterCellStyle);
  }

  private setCellStyle(cell: XLSX.CellObject, style: {}) {
    cell.s = style;
  }

  public generaReporteReversa() {
    this.excelService.exportAsExcelFile(this.listadolistadoReporteReversa, 'ListadoReporteReversa');
  }

  limpiarInfo() {
    this.credito = '';
    this.fechaInicio = '';
    this.fechaFin = '';
    this.dataSourceReporteReversa =
    new MatTableDataSource<ListadoReporteReversas>();
    this.isHiddenBtnGenExcel = true;

  }
  validarInput() {
    if ((this.credito !== '' && this.credito !== undefined)
        || ((this.fechaInicio !== '' && this.fechaInicio !== undefined &&
              this.fechaInicio !== null) &&
             (this.fechaFin !== '' && this.fechaFin !== undefined &&
              this.fechaFin !== null))) {
                this.traeReporteReversa();
             } else {
               this.alertServices.warn('Debe Indicar el credito o ambas fechas para la busqueda');
             }
  }

}
