import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteDesaplicacionPagoComponent } from './reporte-desaplicacion-pago.component';

describe('ReporteDesaplicacionPagoComponent', () => {
  let component: ReporteDesaplicacionPagoComponent;
  let fixture: ComponentFixture<ReporteDesaplicacionPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteDesaplicacionPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteDesaplicacionPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
