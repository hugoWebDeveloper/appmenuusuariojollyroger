import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatCheckboxChange } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListadoBitacoraReversas } from 'src/app/models/listado-bitacora-reversa';
import { ExcelService } from 'src/app/services/excel-service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XLSXStyle from 'xlsx-style';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export class SalidaExcel {
  id: number;
  fecha: string;
  mensaje: string;
  idreversa: number;
  idcredito: string;
  folio: string;
  estatus: number;
  usuarioCreacion: string;
  fechaCreacion: string;
}
@Component({
  selector: 'app-bitacora-desaplicacion-pago',
  templateUrl: './bitacora-desaplicacion-pago.component.html',
  providers: [DesaplicacionPagoService, ExcelService]
})
export class BitacoraDesaplicacionPagoComponent implements OnInit {
  title: string;
  formulario: FormGroup;
  fechaInicio: string;
  fechaFin: string;
  spinner: boolean;
  isHiddenBtnGenExcel = true;
  displayedColumns: string[] = ['id', 'fecha', 'mensaje', 'idreversa', 'idcredito', 'folio',
                                 'estatus',  'usuariocreacion', 'fechacreacion'];
   public dataSourceBitacoraExitosos: MatTableDataSource<ListadoBitacoraReversas>;
   public dataSourceBitacoraFallidos: MatTableDataSource<ListadoBitacoraReversas>;

   elementoFiltroReporte  = {
    fechaInicio: '',
    fechaFin: ''
  };

  public elementoExcel: SalidaExcel;
  public listadoBitacoraReversas: ListadoBitacoraReversas;
  public listadolistadoBitacoraReversas: ListadoBitacoraReversas[];
  constructor(private fb: FormBuilder, private alertServices: AlertService,
              private desaplicacionPagoService: DesaplicacionPagoService,
              private cdr: ChangeDetectorRef,
              public dialog: MatDialog,
              private excelService: ExcelService
    ) {
      this.formulario = this.fb.group({
        fechaInicio: ['', [Validators.required]],
        fechaFin: ['', [Validators.required]]
    });

    }

  ngOnInit() {
    this.title = 'Desaplicacion Pago - Bitacora';
  }
  buscarBitacora() {
    if (this.fechaInicio !== '' && this.fechaInicio !== undefined &&
    this.fechaInicio !== null) {
    this.elementoFiltroReporte.fechaInicio = this.convertirFechasInicio(this.fechaInicio);
    } else {
    this.elementoFiltroReporte.fechaInicio = null;
    }

    if (this.fechaFin !== '' && this.fechaFin !== undefined &&
      this.fechaFin !== null) {
      this.elementoFiltroReporte.fechaFin = this.convertirFechasFin(this.fechaFin);
    } else {
      this.elementoFiltroReporte.fechaFin = null;
    }
    this.desaplicacionPagoService.ConsultaBitacoraReversa(this.elementoFiltroReporte).subscribe(
        Response => {
          this.listadolistadoBitacoraReversas = Response.returns.data.content;
          console.log('bitacora');
          console.log(this.listadolistadoBitacoraReversas);
          this.listadolistadoBitacoraReversas.forEach(elemento => {
            elemento.fecha = elemento.fecha.substring(0, 10);
          });
          this.dataSourceBitacoraExitosos =
          new MatTableDataSource<ListadoBitacoraReversas>(this.listadolistadoBitacoraReversas.
          filter(elemento => elemento.reversa.estatus === 2));
          this.dataSourceBitacoraFallidos =
          new MatTableDataSource<ListadoBitacoraReversas>(this.listadolistadoBitacoraReversas.
          filter(elemento => elemento.reversa.estatus === 3));
          this.alertServices.success('consulta exitosa');
          this.isHiddenBtnGenExcel = false;
        }, error => {
          if (error.error.status === 400) {
            this.spinner = false;
            // this.resultsLength = 0;
            this.dataSourceBitacoraExitosos =
            new MatTableDataSource<ListadoBitacoraReversas>();
            this.dataSourceBitacoraFallidos =
            new MatTableDataSource<ListadoBitacoraReversas>();
            this.alertServices.error(error.errors[0]);
          } else {
            this.spinner = false;
            this.dataSourceBitacoraExitosos =
            new MatTableDataSource<ListadoBitacoraReversas>();
            this.dataSourceBitacoraFallidos =
            new MatTableDataSource<ListadoBitacoraReversas>();
            // this.resultsLength = 0;
            this.alertServices.error(error.error.message);
          }
        }
      );
  }

  convertirFechasInicio(parametro) {
    console.log(parametro);
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-') + 'T00:00:00.000Z';
  }

  convertirFechasFin(parametro) {
    console.log(parametro);
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-') + 'T23:59:59.000Z';
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: {}, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private wrapAndCenterCell(cell: XLSX.CellObject) {
    const wrapAndCenterCellStyle = { alignment: { wrapText: true, vertical: 'center', horizontal: 'center' } };
    this.setCellStyle(cell, wrapAndCenterCellStyle);
  }

  private setCellStyle(cell: XLSX.CellObject, style: {}) {
    cell.s = style;
  }

  public generaReporteBitacora() {
    const listadoBitacoraReversasExcel = new Array();
    this.listadolistadoBitacoraReversas.forEach(elemento => {
      this.elementoExcel = new SalidaExcel();
      this.elementoExcel.id = elemento.id;
      this.elementoExcel.fecha = elemento.fecha;
      this.elementoExcel.mensaje = elemento.mensaje;
      this.elementoExcel.usuarioCreacion = elemento.usuarioCreacion;
      this.elementoExcel.fechaCreacion = elemento.fechaCreacion;
      this.elementoExcel.idreversa = elemento.reversa.id;
      this.elementoExcel.idcredito = elemento.reversa.id_credito;
      this.elementoExcel.folio = elemento.reversa.folio;
      this.elementoExcel.estatus = elemento.reversa.estatus;
      listadoBitacoraReversasExcel.push(this.elementoExcel);
    });
    this.excelService.exportAsExcelFile(listadoBitacoraReversasExcel, 'ListadoReporteBitacora');
  }

  limpiarInfo() {
    this.fechaInicio = '';
    this.fechaFin = '';
    this.dataSourceBitacoraExitosos =
          new MatTableDataSource<ListadoBitacoraReversas>();
    this.dataSourceBitacoraFallidos =
          new MatTableDataSource<ListadoBitacoraReversas>();
    this.isHiddenBtnGenExcel = true;

  }

  convertirFechasReporte(parametro) {
    console.log(parametro);
    const dateDay = new Date();
    console.log(dateDay.getTime());
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    const hora = ('0' + (dateDay.getHours() )).slice(-2);
    const minuto = ('0' + (dateDay.getMinutes() )).slice(-2);
    const segundo = ('0' + (dateDay.getSeconds() )).slice(-2);
    const tiempo = hora + ':' + minuto + ':' + segundo;
    console.log([date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000Z');
    return [date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000';
  }
}


