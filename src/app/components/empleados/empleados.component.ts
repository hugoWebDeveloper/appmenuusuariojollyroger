import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '@next/nx-controls-common';
import { timer } from 'rxjs';
import { empleado, EmpleadoService } from 'src/app/services/empleado.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.scss']
})
export class EmpleadosComponent implements OnInit {
  empleadoForm: FormGroup;
  empleado: empleado;
  empleados: empleado[];
  idEmpleado: number = 0;
  btnMensaje: string;
  mostrarAviso = false;
  mensajeAviso: string;

  constructor(
    private empleadoService: EmpleadoService,
    private alertService: AlertService
  ) { }

  ngOnInit() {

    this.btnMensaje = "Agregar";
    this.obtener();

    this.empleadoForm = new FormGroup({
      nombre: new FormControl('', [Validators.required]),
      brm: new FormControl('', [Validators.required]),
      puesto: new FormControl('Desarrollador FrontEnd'),
      foto: new FormControl('')
    });
  }

  onSubmit() {

    //console.log(this.empleadoForm.get("foto").value);
    const empleado: empleado = {
      id: this.idEmpleado != 0 ? this.idEmpleado : null,
      nombre: this.empleadoForm.get("nombre").value,
      brm: this.empleadoForm.get("brm").value,
      puesto: this.empleadoForm.get("puesto").value,
      foto: this.empleadoForm.get("foto").value
    }

    if (this.idEmpleado != 0) {
      this.empleadoService.editar(empleado)
        .subscribe((result) => {

          if (result != null) {
            this.obtener();
            this.btnMensaje = "Agregar";
            //this.mostrarAviso = true;
            //this.mensajeAviso = "Se ha editado correctamente."
            this.idEmpleado = 0;
            this.alertService.success("Se ha editado correctamente.");
          }
        });

    } else {
      this.empleadoService.agregar(empleado)
        .subscribe((result: empleado) => {

          if (result != null) {
            this.obtener();
            //this.mostrarAviso = true;
            //this.mensajeAviso = "Se ha agregado correctamente."
            this.alertService.success("Se ha agregado correctamente.");
          }
        });
    }

    /*timer(3000).subscribe(result => {
      this.mostrarAviso = false;
      this.mensajeAviso = ""
    });*/
    this.empleadoForm.reset();
    this.empleadoForm.get("puesto").setValue("Desarrollador FrontEnd");
  }


  obtener() {
    this.empleadoService.obtener()
      .subscribe((result: empleado[]) => {
        this.empleados = result;
        console.log(result)
      });
  }


  eliminar(id: number) {
    if (confirm("¿Desea borrar el registro?")) {
      this.empleadoService.eliminar(id)
        .subscribe(result => {
          this.obtener();
          //this.mostrarAviso = true;
          //this.mensajeAviso = "Se ha eliminado correctamente."
          this.alertService.success("Se ha eliminado correctamente.");
          /*timer(3000).subscribe(result => {
            this.mostrarAviso = false;
            this.mensajeAviso = ""
          });*/
        });
    }
  }

  editar(item) {
    this.btnMensaje = "Editar";
    this.idEmpleado = item.id;
    this.empleadoForm.get("nombre").setValue(item.nombre);
    this.empleadoForm.get("puesto").setValue(item.puesto);
    this.empleadoForm.get("brm").setValue(item.brm);

  }

  //Para convertir imagen a base64
  convertirBase64(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

      this.empleadoForm.get("foto").setValue(reader.result);

    };

  }
}
