import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalComentarioAutorizarRechazarComponent } from './modal-comentario-autorizar-rechazar.component';

describe('ModalComentarioAutorizarRechazarComponent', () => {
  let component: ModalComentarioAutorizarRechazarComponent;
  let fixture: ComponentFixture<ModalComentarioAutorizarRechazarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComentarioAutorizarRechazarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComentarioAutorizarRechazarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
