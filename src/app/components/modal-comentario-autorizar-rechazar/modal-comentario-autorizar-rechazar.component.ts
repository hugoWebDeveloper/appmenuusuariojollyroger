import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';

@Component({
  selector: 'app-modal-comentario-autorizar-rechazar',
  templateUrl: './modal-comentario-autorizar-rechazar.component.html',
  styleUrls: ['./modal-comentario-autorizar-rechazar.component.scss'],
  providers: [DesaplicacionPagoService]
})
export class ModalComentarioAutorizarRechazarComponent implements OnInit {
  comentario: string;
  titulo: string;
  proceso: number;
  formulario: FormGroup;
  idReversa: 0;
  spinner: boolean;
  mensaje: string;
  bandera: number;

  elementoNodosTareas = {
    camino: 'CONTINUAR',
    nodoId: '',
    procesoId: '',
    tareaId: ''
  };
  elementoBitacora = {
    fecha: '',
    id: 0,
    mensaje: '',
    reversa: {id : 0}
  };
  elementoDesactivaTarea = {
    estatus: 0,
    folio: '0',
    id: 0,
    id_credito: '000000000000000',
    numero_transaccion: '0000000000'
  };

  constructor(private fb: FormBuilder, private alertServices: AlertService, public dialog: MatDialog,
              private dialogModalComentario: MatDialogRef<ModalComentarioAutorizarRechazarComponent>,
              private desaplicacionPagoService: DesaplicacionPagoService,
              @Inject(MAT_DIALOG_DATA) private data: any) {
        this.formulario = this.fb.group({
        comentario: [''],
        });
     }

  ngOnInit() {
    this.titulo = this.dialogModalComentario.componentInstance.data.titulo[0].tipoTitulo;
    this.proceso = this.dialogModalComentario.componentInstance.data.titulo[0].proceso;
    this.elementoNodosTareas.nodoId = this.dialogModalComentario.componentInstance.data.folioSeleccionado.nodo;
    this.elementoNodosTareas.procesoId = this.dialogModalComentario.componentInstance.data.folioSeleccionado.proceso;
    this.elementoNodosTareas.tareaId = this.dialogModalComentario.componentInstance.data.folioSeleccionado.tarea;
    this.elementoBitacora.reversa.id = this.dialogModalComentario.componentInstance.data.idreversa;
    this.bandera = 0;
    console.log(this.elementoNodosTareas);
    console.log(this.elementoBitacora.reversa.id);
  }

  onNoClick(): void {
    this.dialogModalComentario.close();
  }
  guardarComentario() {
    if ((this.comentario === undefined || this.comentario === '') && this.proceso === 2) {
      this.alertServices.warn('Favor de capturar un comentario');
    } else {
      this.registroBitacora();
    }
  }

  registroBitacora() {

    if (this.proceso === 2) {
      this.mensaje = 'Rechazo aplicado';
      this.elementoNodosTareas.camino = 'RECHAZAR';
  } else {
    this.mensaje = 'Autorizacion aplicado';
  }
    this.desaplicacionPagoService.altaNodoProceso(this.elementoNodosTareas).subscribe(
      Response => {
      if  (this.proceso === 2) {
        // se llama solamente cuando es rechazo
        this.finalizarTarea(Response);
      } else {
        // se llama este flujo cuando es autorizacion
        this.alertServices.success(this.mensaje);
        this.guardaBitacora();
      }
      console.log(Response);
     }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.alertServices.error(error.error.internalmessage[0].description);
          return false;
        } else {
          this.spinner = false;
          this.alertServices.error(error.error.message);
          return false;
        }

      }
    );
  }
  convertirFechas(parametro) {
    console.log(parametro);
    console.log(parametro.getTime());
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    const hora = ('0' + (date.getHours() )).slice(-2);
    const minuto = ('0' + (date.getMinutes() )).slice(-2);
    const segundo = ('0' + (date.getSeconds() )).slice(-2);
    const tiempo = hora + ':' + minuto + ':' + segundo;
    console.log([date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000Z');
    return [date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000Z';
  }

  guardaBitacora() {
    if (this.comentario === undefined ) {
      this.comentario = ' ';
    }
    const dateDay = new Date();
    this.elementoBitacora.fecha = this.convertirFechas(dateDay);
    this.elementoBitacora.mensaje = this.comentario;
    this.desaplicacionPagoService.altabitacora(this.elementoBitacora).subscribe(
      Response => {
        this.alertServices.success('Registro de bitacora exitosa');
        this.onNoClick();
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.alertServices.error(error.error.internalmessage[0].description);
          return false;
        } else {
          this.spinner = false;
          this.alertServices.error(error.error.message);
          return false;
        }
      }
    );
  }
  finalizarTarea(response) {
      this.desaplicacionPagoService.consultarProcesosTareas(response.numero).subscribe(
          Response => {
            this.elementoNodosTareas.nodoId = Response.idNodo;
            this.elementoNodosTareas.procesoId = Response.idProceso;
            this.elementoNodosTareas.tareaId = Response.idTarea;
            this.llamaFinalizacion();
          }, error => {
            if (error.error.status === 400) {
              this.spinner = false;
              this.alertServices.error(error.error.internalmessage[0].description);
              return false;
            } else {
              this.spinner = false;
              this.alertServices.error(error.error.message);
              return false;
            }
          }
      );
  }
  llamaFinalizacion() {
    this.desaplicacionPagoService.altaNodoProceso(this.elementoNodosTareas).subscribe(
      Response => {
        this.cambiaEstatusRechazo();
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.alertServices.error(error.error.internalmessage[0].description);
          return false;
        } else {
          this.spinner = false;
          this.alertServices.error(error.error.message);
          return false;
        }
      }
    );
  }
  cambiaEstatusRechazo() {
    this.elementoDesactivaTarea.estatus = 3;
    this.desaplicacionPagoService.cambioStatus(this.elementoBitacora.reversa.id, this.elementoDesactivaTarea).subscribe(
      Response => {
        this.guardaBitacora();
        this.alertServices.success(this.mensaje);
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.alertServices.error(error.error.internalmessage[0].description);
          return false;
        } else {
          this.spinner = false;
          this.alertServices.error(error.error.message);
          return false;
        }
      }
    );
  }

}
