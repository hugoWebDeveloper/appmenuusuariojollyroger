import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAltaMotivoDesaplicacionComponent } from './modal-alta-motivo-desaplicacion.component';

describe('ModalAltaMotivoDesaplicacionComponent', () => {
  let component: ModalAltaMotivoDesaplicacionComponent;
  let fixture: ComponentFixture<ModalAltaMotivoDesaplicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAltaMotivoDesaplicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAltaMotivoDesaplicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
