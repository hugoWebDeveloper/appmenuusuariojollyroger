import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-modal-alta-motivo-desaplicacion',
  templateUrl: './modal-alta-motivo-desaplicacion.component.html',
  styleUrls: ['./modal-alta-motivo-desaplicacion.component.scss'],
  providers: [DesaplicacionPagoService]
})
export class ModalAltaMotivoDesaplicacionComponent implements OnInit {

  accion: number;
  formulario: FormGroup;
  spinner: boolean;
  color: string;
  checkActivo: boolean;
  id: number;
  motivo: string;
  abreviatura: string;

  elementoAltaEdicion = {
    abreviatura: '',
    activo: false,
    nombre: '',
    id: 0
  };

  constructor(private fb: FormBuilder, private alertServices: AlertService, public dialog: MatDialog,
              private dialogAltamotivoDesaplicacion: MatDialogRef<ModalAltaMotivoDesaplicacionComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private desaplicacionPagoService: DesaplicacionPagoService) {
                this.formulario = this.fb.group({
                  motivo: ['', [Validators.required]],
                 abreviatura: ['', [Validators.required]],
                  check : ['']
                  });
              }

  ngOnInit() {

    this.spinner = false;
    this.color = 'warn';
    this.accion = this.dialogAltamotivoDesaplicacion.componentInstance.data.accion;
    this.checkActivo = true;
    if (this.accion === 1) {
      this.id = this.dialogAltamotivoDesaplicacion.componentInstance.data.info.id;
      this.motivo = this.dialogAltamotivoDesaplicacion.componentInstance.data.info.nombre;
      this.abreviatura = this.dialogAltamotivoDesaplicacion.componentInstance.data.info.abreviatura;
      this.checkActivo = this.dialogAltamotivoDesaplicacion.componentInstance.data.info.activo;
    }
  }
  onNoClick(): void {
    this.dialogAltamotivoDesaplicacion.close();
  }
public agregarMotivoDesaplicacion() {
    if (this.accion === 1) {
      this.editarMotivoDesaplicacion();
      return false;
    }
    this.elementoAltaEdicion.nombre = this.motivo;
    this.elementoAltaEdicion.activo = this.checkActivo;
    this.elementoAltaEdicion.abreviatura = this.abreviatura;
    this.spinner = true;
    this.desaplicacionPagoService.altaMotivoDesaplicacion(this.elementoAltaEdicion)
          .subscribe(
            response => {
              if ( response.errors !== undefined ) {
                this.alertServices.warn(response.errors[0].description);
                return;
              }
              if (response.success !== undefined) {
                if (response.returns.data.length === 0) {
                  this.alertServices.warn('No Existen Datos');
                  return;
                } else {
                  this.alertServices.success('Alta Exitosa');
                  return;
                }
              }
              }, error => {
                console.log(error);
                if (error.error.status === 400) {
                  this.spinner = false;
                  this.alertServices.error(error.error.internalmessage[0].description);
                  return false;
                } else {
                  this.spinner = false;
                  this.alertServices.error(error.error.message);
                  return false;
                }
              }, () => {
                this.spinner = false;
                this.onNoClick();
              }
          );
  }

  public editarMotivoDesaplicacion() {
    this.elementoAltaEdicion.id = this.id;
    this.elementoAltaEdicion.nombre = this.motivo;
    this.elementoAltaEdicion.abreviatura = this.abreviatura;
    this.elementoAltaEdicion.activo = this.checkActivo;
    this.spinner = true;
    this.desaplicacionPagoService.editarMotivoDesaplicacion(this.id, this.elementoAltaEdicion)
          .subscribe(
            response => {
              if ( response.errors !== undefined ) {
                this.alertServices.warn(response.errors[0].description);
                return;
              }
              if (response.success !== undefined) {
                if (response.returns.data.length === 0 ) {
                  this.alertServices.warn('No Existen Datos');
                  return;
                } else {
                  this.alertServices.success('Actualización Exitosa');
                  return;
                }
              }
              }, error => {
                console.log(error);
                if (error.error.status === 400) {
                  this.spinner = false;
                  this.alertServices.error(error.error.internalmessage[0].description);
                  return false;
                } else {
                  this.spinner = false;
                  this.alertServices.error(error.error.message);
                  return false;
                }
              }, () => {
                this.spinner = false;
                this.onNoClick();
              }
          );
  }

  zfill( numbero: any, width: any ) {
    const numberOutput = Math.abs(numbero);
    const length = numbero.toString().length;
    const zero = '0';
    if (width <= length) {
        if (numbero < 0) {
             return ('-' + numberOutput.toString());
        } else {
             return numberOutput.toString();
        }
    } else {
        if (numbero < 0) {
            return ('-' + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
  }
}
