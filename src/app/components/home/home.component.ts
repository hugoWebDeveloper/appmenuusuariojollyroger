import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: []
})
export class HomeComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit() {

  }

  
  leftContent = [
    { description: 'Menú', isTitle: true },
    { description: 'Empleado', isTitle: false, route:"home/empleado" },
    { description: 'Juego', isTitle: false, route:"home/juego" },
    { description: 'Salir', isTitle: false, route:"/milogin" }
  ];

  mainContent = [
    { isHeader: false,
      content: [
        { content: 'Home', isTitle: true }
      ]
    }
  ];

  
  public logout(event: any) {

  }
}
