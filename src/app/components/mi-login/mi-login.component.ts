import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MiLoginService, Usuario } from 'src/app/services/mi-login.service';

@Component({
  selector: 'app-mi-login',
  templateUrl: './mi-login.component.html',
  styleUrls: ['./mi-login.component.scss']
})
export class MiLoginComponent implements OnInit {

  usuario: string;
  contrasena: string;

  constructor(
    private loginService: MiLoginService,
    private router: Router
  ) { }

  ngOnInit() {
    localStorage.removeItem('user');
    localStorage.removeItem('inicioSesion');
    localStorage.removeItem('rol');
  }

  
  iniciarSesion() {

    const usuario: Usuario = {
      usuario: this.usuario,
      contrasena: this.contrasena
    }

    this.loginService.iniciarSesion(usuario)
      .subscribe(result => {
        if (result != null) {

          localStorage.setItem('user',result.usuario);
          localStorage.setItem('inicioSesion', 'true');

          if (result.rolUsuario === 'empleado') {
            this.router.navigate(['/home/empleado']);
            localStorage.setItem('rol', 'empleado');

          }
          if (result.rolUsuario === 'jugador') {
            this.router.navigate(['/home/juego']);
            localStorage.setItem('rol', 'jugador');
          }

        } else {
          alert("Usuario o Contraseña incorrectas");
        }
      });
  }
}
