import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiLoginComponent } from './mi-login.component';

describe('MiLoginComponent', () => {
  let component: MiLoginComponent;
  let fixture: ComponentFixture<MiLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
