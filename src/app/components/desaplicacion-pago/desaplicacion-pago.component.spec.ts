import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesaplicacionPagoComponent } from './desaplicacion-pago.component';

describe('DesaplicacionPagoComponent', () => {
  let component: DesaplicacionPagoComponent;
  let fixture: ComponentFixture<DesaplicacionPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesaplicacionPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesaplicacionPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
