export class ListadoBitacoraReversas {
id: number;
    reversa: {
        id: number;
        id_credito: string;
        folio: string;
        estatus: number;
    };
    fecha: string;
    mensaje: string;
    usuarioCreacion: string;
    fechaCreacion: string;

}
