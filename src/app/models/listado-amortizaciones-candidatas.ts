export class ListadoAmortizacionesCandidatas {
    movcredito: string;
    amortizacion: string;
    cantidad: number;
    descripcion: string;
    fecha: string;
    referencia: string;
    tipo: string;
    transaccion: string;
    activo: boolean;
    cantidadStr: string;
}
