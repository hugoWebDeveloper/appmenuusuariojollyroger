export class ListadoTareas {
    folio: string;
    tarea: string;
    atiende: string;
    fecha: string;
    solicitante: string;
    nodo: string;
    proceso: string;
    perfil: string;
}
