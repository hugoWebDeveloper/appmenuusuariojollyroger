export class ListadoReporteReversas {
    movcredito: string;
    numeroTransaccion: string;
    usuario: string;
    idCliente: string;
    nombreCliente: string;
    tipoLinea: string;
    descripcioncLinea: string;
    cuenta: string;
    fechaOriginal: string;
    cantidadOriginal: number;
    fechaReversa: string;
    cantidadReversa: number;
    tipoMovimiento: string;
    descripcionMovimiento: string;
    amortizacion: string;
    fechaPagoAmortizacion: string;
    fechaVencimiento: string;
    statusAmortizacion: string;
    clasificacion: string;
}

