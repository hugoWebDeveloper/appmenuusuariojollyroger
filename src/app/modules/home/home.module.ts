import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from 'src/app/components/home/home.component';



import { PantallaComponent } from 'src/app/components/pantalla/pantalla.component';
import { MotivoDesaplicacionComponent } from 'src/app/components/motivo-desaplicacion/motivo-desaplicacion.component';


import { FormsModule } from '@angular/forms';
import { FrameworkModule} from '@next/nx-core';
import { CommonsModule} from '@next/nx-controls-common';

import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatInputModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule, MatRadioModule,
  MatDatepickerModule, MatNativeDateModule, MatTableModule, MatIconModule, MatButtonToggleModule, MatSlideToggleModule,
  MatTabsModule, MatCheckboxModule, MatMenuModule, MatStepperModule, MatPaginatorModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';


import { MatFormFieldModule } from '@angular/material';

import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';


const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '/api',
  application: 'DESAPLICACIONREVERSA',
  applicationTitle: 'Desaplicacion de Pagos'
};


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot()
  ]
})
export class HomeModule { }
