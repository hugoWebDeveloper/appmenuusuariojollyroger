import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
  basePath = '/seguridad';
  constructor(private http: HttpClient) { }

  /**
   * Cierra sesion
   */
  cerrarSesion(): Observable<any > {
      return this.http.delete<any>(`${this.basePath}/login`, {});
  }
}
