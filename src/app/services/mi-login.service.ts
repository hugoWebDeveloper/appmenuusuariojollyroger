import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MiLoginService {

  api = "https://app-menu-usuarios-back.herokuapp.com/api";

  constructor(
    private http: HttpClient
  ) { }

  iniciarSesion(usuario: Usuario){
    return this.http.post<Usuario>(this.api+"/login",usuario);
   }
   
}

export interface Usuario{
  id?:number,
  usuario: string,
  contrasena: string,
  rolUsuario?:string
}