import { TestBed } from '@angular/core/testing';

import { MiLoginService } from './mi-login.service';

describe('MiLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MiLoginService = TestBed.get(MiLoginService);
    expect(service).toBeTruthy();
  });
});
