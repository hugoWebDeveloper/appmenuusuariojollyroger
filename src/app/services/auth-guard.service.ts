import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { MiLoginService } from './mi-login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(
    private router: Router,
    private loginService: MiLoginService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const texto: any = route.data.role;

    if (localStorage.getItem('rol') == route.data.role) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {



    if (localStorage.getItem('rol') == childRoute.data.role) {
      return true;
    } else {
      this.router.navigate(['noAutorizado']);
      return false;
    }
  }
}
